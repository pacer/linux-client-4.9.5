/*
 * SME Linux Security Module
 *
 * created on: Jan 30, 2017
 * author: aasthakm
 */

#include <linux/module.h>
#include <linux/lsm_hooks.h>

int sme_enabled = 1;

// === LSM Hooks ===
int
(*lnk_socket_create) (int family, int type, int protocol, int kern) = 0;
EXPORT_SYMBOL(lnk_socket_create);
static int
sme_lsm_socket_create(int family, int type, int protocol, int kern)
{
	int ret;
	if (lnk_socket_create) {
		ret = lnk_socket_create(family, type, protocol, kern);
	} else {
		ret = 0;
	}
	return ret;
}

int
(*lnk_socket_post_create) (struct socket *sock, int family, int type,
		int protocol, int kern) = 0;
EXPORT_SYMBOL(lnk_socket_post_create);
static int 
sme_lsm_socket_post_create(struct socket *sock, int family, int type,
		int protocol, int kern)
{
	int ret;
	if (lnk_socket_post_create) {
		ret = lnk_socket_post_create(sock, family, type, protocol, kern);
	} else {
		ret = 0;
	}
	return ret;
}

int
(*lnk_socket_bind) (struct socket *sock, struct sockaddr *address, int addrlen) = 0;
EXPORT_SYMBOL(lnk_socket_bind);
static int
sme_lsm_socket_bind(struct socket *sock, struct sockaddr *address, int addrlen)
{
	int ret;
	if (lnk_socket_bind) {
		ret = lnk_socket_bind(sock, address, addrlen);
	} else {
		ret = 0;
	}
	return ret;
}

int
(*lnk_socket_connect) (struct socket *sock, struct sockaddr *address, 
		int addrlen) = 0;
EXPORT_SYMBOL(lnk_socket_connect);
static int
sme_lsm_socket_connect(struct socket *sock, struct sockaddr *address, int addrlen)
{
	int ret;
	if (lnk_socket_connect) {
		ret = lnk_socket_connect(sock, address, addrlen);
	} else {
		ret = 0;
	}
	return ret;
}

int
(*lnk_socket_listen) (struct socket *sock, int backlog) = 0;
EXPORT_SYMBOL(lnk_socket_listen);
static int
sme_lsm_socket_listen(struct socket *sock, int backlog)
{
	int ret;
	if (lnk_socket_listen) {
		ret = lnk_socket_listen(sock, backlog);
	} else {
		ret = 0;
	}
	return ret;
}

int
(*lnk_socket_accept) (struct socket *sock, struct socket *newsock) = 0;
EXPORT_SYMBOL(lnk_socket_accept);
static int
sme_lsm_socket_accept(struct socket *sock, struct socket *newsock)
{
	int ret;
	if (lnk_socket_accept) {
		ret = lnk_socket_accept(sock, newsock);
	} else {
		ret = 0;
	}
	return ret;
}

int
(*lnk_socket_sendmsg) (struct socket *sock, struct msghdr *msg, int size) = 0;
EXPORT_SYMBOL(lnk_socket_sendmsg);
static int
sme_lsm_socket_sendmsg(struct socket *sock, struct msghdr *msg, int size)
{
	int ret;
	if (lnk_socket_sendmsg) {
		ret = lnk_socket_sendmsg(sock, msg, size);
	} else {
		ret = 0;
	}
	return ret;
}

int
(*lnk_socket_recvmsg) (struct socket *sock, struct msghdr *msg, int size,
		int flags) = 0;
EXPORT_SYMBOL(lnk_socket_recvmsg);
static int
sme_lsm_socket_recvmsg(struct socket *sock, struct msghdr *msg, int size, int flags)
{
	int ret;
	if (lnk_socket_recvmsg) {
		ret = lnk_socket_recvmsg(sock, msg, size, flags);
	} else {
		ret = 0;
	}
	return ret;
}

int
(*lnk_socket_getsockopt) (struct socket *sock, int level, int optname) = 0;
EXPORT_SYMBOL(lnk_socket_getsockopt);
static int
sme_lsm_socket_getsockopt(struct socket *sock, int level, int optname)
{
	int ret;
	if (lnk_socket_getsockopt) {
		ret = lnk_socket_getsockopt(sock, level, optname);
	} else {
		ret = 0;
	}
	return ret;
}

int
(*lnk_socket_setsockopt) (struct socket *sock, int level, int optname) = 0;
EXPORT_SYMBOL(lnk_socket_setsockopt);
static int
sme_lsm_socket_setsockopt(struct socket *sock, int level, int optname)
{
	int ret;
	if (lnk_socket_setsockopt) {
		ret = lnk_socket_setsockopt(sock, level, optname);
	} else {
		ret = 0;
	}
	return ret;
}

int
(*lnk_socket_shutdown) (struct socket *sock, int how) = 0;
EXPORT_SYMBOL(lnk_socket_shutdown);
static int
sme_lsm_socket_shutdown(struct socket *sock, int how)
{
	int ret;
	if (lnk_socket_shutdown) {
		ret = lnk_socket_shutdown(sock, how);
	} else {
		ret = 0;
	}
	return ret;
}

static struct security_hook_list sme_hooks[] = {
	LSM_HOOK_INIT(socket_create, sme_lsm_socket_create),
	LSM_HOOK_INIT(socket_post_create, sme_lsm_socket_post_create),
	LSM_HOOK_INIT(socket_bind, sme_lsm_socket_bind),
	LSM_HOOK_INIT(socket_connect, sme_lsm_socket_connect),
	LSM_HOOK_INIT(socket_listen, sme_lsm_socket_listen),
	LSM_HOOK_INIT(socket_accept, sme_lsm_socket_accept),
	LSM_HOOK_INIT(socket_sendmsg, sme_lsm_socket_sendmsg),
	LSM_HOOK_INIT(socket_recvmsg, sme_lsm_socket_recvmsg),
	LSM_HOOK_INIT(socket_getsockopt, sme_lsm_socket_getsockopt),
	LSM_HOOK_INIT(socket_setsockopt, sme_lsm_socket_setsockopt),
	LSM_HOOK_INIT(socket_shutdown, sme_lsm_socket_shutdown),
};

static __init int
sme_init(void)
{
	if (!security_module_enable("sme")) {
		sme_enabled = 0;
		return 0;
	}

	if (!sme_enabled) {
		printk(KERN_EMERG "SME: Disabled at boot\n");
		return 0;
	}

	printk(KERN_EMERG "SME: Initializing\n");

	security_add_hooks(sme_hooks, ARRAY_SIZE(sme_hooks));
	printk(KERN_EMERG "SME install security hooks");

	return 0;
}

security_initcall(sme_init);

#if 0
static int sme_disabled = 0;
int
sme_disable(void)
{
	if (sme_disabled) {
		/* Only do it once */
		return -EINVAL;
	}

	printk(KERN_EMERG "SME: Disabled at runtime\n");
	sme_disabled = 1;
	sme_enabled = 0;
	security_delete_hooks(sme_hooks, ARRAY_SIZE(sme_hooks));
	printk(KERN_EMERG "SME delete security hooks");

	return 0;
}
#endif
