/*
 * Similar to LSM modules like selinux/hooks.c
 * Xen side channel mitigation module
 *
 * created on: Feb 23, 2017
 * author: aasthakm
 */

#include "xen_sme.h"
#include "sme_debug.h"
#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <uapi/linux/tcp.h>
#include <uapi/linux/in.h>
#include <uapi/linux/ip.h>
#include <uapi/asm-generic/errno-base.h>


void
(*lnk_update_cwnd) (struct sk_buff *skb, struct sock *sk, int caller,
    int b_cwnd, int b_pkts, int b_lost, int b_retrans, int b_sack,
    int a_cwnd, int a_pkts, int a_lost, int a_retrans, int a_sack,
    int una_diff, int delivered, int ack_flag, int num_marked_lost,
    char *extra_dbg_string) = 0;
EXPORT_SYMBOL(lnk_update_cwnd);
static void
sme_update_cwnd(struct sk_buff *skb, struct sock *sk, int caller,
    int b_cwnd, int b_pkts, int b_lost, int b_retrans, int b_sack,
    int a_cwnd, int a_pkts, int a_lost, int a_retrans, int a_sack,
    int una_diff, int delivered, int ack_flag, int num_marked_lost,
    char *extra_dbg_string)
{
  if (lnk_update_cwnd) {
    lnk_update_cwnd(skb, sk, caller, b_cwnd, b_pkts, b_lost, b_retrans, b_sack,
        a_cwnd, a_pkts, a_lost, a_retrans, a_sack, una_diff, delivered, ack_flag,
        num_marked_lost, extra_dbg_string);
  }
}

int
(*lnk_intercept_select_queue) (struct net_device *dev, struct sk_buff *skb,
    char *extra_dbg_string) = 0;
EXPORT_SYMBOL(lnk_intercept_select_queue);
static int
sme_intercept_select_queue(struct net_device *dev, struct sk_buff *skb,
    char *extra_dbg_string)
{
  int ret = -ENODEV;
  if (lnk_intercept_select_queue) {
    ret = lnk_intercept_select_queue(dev, skb, extra_dbg_string);
  }
  return ret;
}

void
(*lnk_intercept_tx_sent_queue) (struct netdev_queue *txq, unsigned int bytes) = 0;
EXPORT_SYMBOL(lnk_intercept_tx_sent_queue);
static void
sme_intercept_tx_sent_queue(struct netdev_queue *txq, unsigned int bytes)
{
  if (lnk_intercept_tx_sent_queue) {
    lnk_intercept_tx_sent_queue(txq, bytes);
  }
  return;
}

void
(*lnk_intercept_tx_completed_queue) (struct netdev_queue *txq,
    unsigned int pkts, unsigned int bytes) = 0;
EXPORT_SYMBOL(lnk_intercept_tx_completed_queue);
static void
sme_intercept_tx_completed_queue(struct netdev_queue *txq, unsigned int pkts,
    unsigned int bytes)
{
  if (lnk_intercept_tx_completed_queue) {
    lnk_intercept_tx_completed_queue(txq, pkts, bytes);
  }

  return;
}

int
(*lnk_intercept_sk_buff) (struct sk_buff *skb, char *extra_dbg_string) = 0;
EXPORT_SYMBOL(lnk_intercept_sk_buff);
static int
sme_intercept_sk_buff(struct sk_buff *skb, char *extra_dbg_string)
{
	int ret = -ENODEV;
	if (lnk_intercept_sk_buff) {
		ret = lnk_intercept_sk_buff(skb, extra_dbg_string);
	}

	return ret;
}

void
(*lnk_intercept_xmit_stop_queue) (struct net_device *dev, struct netdev_queue *txq,
    struct bnx2x_fp_txdata *txdata) = 0;
EXPORT_SYMBOL(lnk_intercept_xmit_stop_queue);
static void
sme_intercept_xmit_stop_queue(struct net_device *dev, struct netdev_queue *txq,
    struct bnx2x_fp_txdata *txdata)
{
  if (lnk_intercept_xmit_stop_queue) {
    lnk_intercept_xmit_stop_queue(dev, txq, txdata);
  }
}

int
(*lnk_intercept_tx_int) (struct net_device *dev, struct bnx2x_fp_txdata *txdata) = 0;
EXPORT_SYMBOL(lnk_intercept_tx_int);
static int
sme_intercept_tx_int(struct net_device *dev, struct bnx2x_fp_txdata *txdata)
{
  int ret = -1;
  if (lnk_intercept_tx_int) {
    ret = lnk_intercept_tx_int(dev, txdata);
  }
  return ret;
}

void
(*lnk_intercept_free_tx_skbs_queue) (struct bnx2x_fastpath *fp) = 0;
EXPORT_SYMBOL(lnk_intercept_free_tx_skbs_queue);
static void
sme_intercept_free_tx_skbs_queue(struct bnx2x_fastpath *fp)
{
  if (lnk_intercept_free_tx_skbs_queue) {
    lnk_intercept_free_tx_skbs_queue(fp);
  }
}

void
(*lnk_parse_rx_data) (void *dev, int fp_idx, uint16_t rx_bd_prod,
		uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons,
		char *data, int data_len, char *extra_dbg_string) = 0;
EXPORT_SYMBOL(lnk_parse_rx_data);
static void
sme_parse_rx_data(void *dev, int fp_idx, uint16_t rx_bd_prod,
		uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons,
		char *data, int data_len, char *extra_dbg_string)
{
	if (lnk_parse_rx_data) {
		lnk_parse_rx_data(dev, fp_idx, rx_bd_prod, rx_bd_cons, comp_prod,
				comp_cons, data, data_len, extra_dbg_string);
	}
}

void
(*lnk_print_sk_buff) (struct sk_buff *skb, struct sock *sk, char *extra_dbg_string) = 0;
EXPORT_SYMBOL(lnk_print_sk_buff);
static void
sme_print_sk_buff(struct sk_buff *skb, struct sock *sk, char *extra_dbg_string)
{
	if (lnk_print_sk_buff) {
		lnk_print_sk_buff(skb, sk, extra_dbg_string);
	}
}

void
(*lnk_intercept_rx_path) (void *dev, int fp_idx, uint16_t rx_bd_prod,
		uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons, int irq,
		char *extra_dbg_string) = 0;
EXPORT_SYMBOL(lnk_intercept_rx_path);
static void
sme_intercept_rx_path(void *dev, int fp_idx, uint16_t rx_bd_prod,
		uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons, int irq,
		char *extra_dbg_string)
{
	if (lnk_intercept_rx_path) {
		lnk_intercept_rx_path(dev, fp_idx, rx_bd_prod, rx_bd_cons,
				comp_prod, comp_cons, irq, extra_dbg_string);
	}
}

void
(*lnk_print_rx_data) (void *dev, int fp_idx, uint16_t rx_bd_prod,
		uint16_t rx_bd_cons, uint16_t rx_comp_prod, uint16_t rx_comp_cons,
		char *extra_dbg_string) = 0;
EXPORT_SYMBOL(lnk_print_rx_data);
static void
sme_print_rx_data(void *dev, int fp_idx, uint16_t rx_bd_prod,
		uint16_t rx_bd_cons, uint16_t rx_comp_prod, uint16_t rx_comp_cons,
		char *extra_dbg_string)
{
	if (lnk_print_rx_data) {
		lnk_print_rx_data(dev, fp_idx, rx_bd_prod, rx_bd_cons,
				rx_comp_prod, rx_comp_cons, extra_dbg_string);
	}
}

struct sme_hook_list xen_sme_hooks[NUM_XEN_SME_HOOKS] = {
	SME_HOOK_INIT(update_cwnd, sme_update_cwnd),
  SME_HOOK_INIT(intercept_select_queue, sme_intercept_select_queue),
  SME_HOOK_INIT(intercept_tx_sent_queue, sme_intercept_tx_sent_queue),
  SME_HOOK_INIT(intercept_tx_completed_queue, sme_intercept_tx_completed_queue),
	SME_HOOK_INIT(intercept_sk_buff, sme_intercept_sk_buff),
  SME_HOOK_INIT(intercept_xmit_stop_queue, sme_intercept_xmit_stop_queue),
  SME_HOOK_INIT(intercept_tx_int, sme_intercept_tx_int),
  SME_HOOK_INIT(intercept_free_tx_skbs_queue, sme_intercept_free_tx_skbs_queue),
	SME_HOOK_INIT(parse_rx_data, sme_parse_rx_data),
	SME_HOOK_INIT(print_sk_buff, sme_print_sk_buff),
	SME_HOOK_INIT(intercept_rx_path, sme_intercept_rx_path),
	SME_HOOK_INIT(print_rx_data, sme_print_rx_data),
};

