#include "xen_sme.h"
#include "xen_sme_hooks.h"
#include "ptcp_hooks.h"
#include "ptcp_hooks_impl.h"
#include "sme_debug.h"
#include <linux/module.h>
#include <linux/ktime.h>

static int __init xen_sme_init(void)
{
	iprintk(0, "SME: Initializing");
#ifdef CONFIG_PACER_BNX2X
	sme_add_hooks(xen_sme_hooks, ARRAY_SIZE(xen_sme_hooks));
#endif

#ifdef CONFIG_PACER_TCP
  ptcp_add_hooks(ptcp_hooks, ARRAY_SIZE(ptcp_hooks));
#endif
	return 0;
}

module_init(xen_sme_init);

static void __exit xen_sme_fini(void)
{
#ifdef CONFIG_PACER_TCP
  ptcp_delete_hooks(ptcp_hooks, ARRAY_SIZE(ptcp_hooks));
#endif

#ifdef CONFIG_PACER_BNX2X
	sme_delete_hooks(xen_sme_hooks, ARRAY_SIZE(xen_sme_hooks));
#endif
	iprintk(0, "SME complete");
}

module_exit(xen_sme_fini);
MODULE_LICENSE("Dual BSD/GPL");
MODULE_ALIAS("xen-backend:sme");
