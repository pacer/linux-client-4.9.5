/*
 * Similar to LSM modules like selinux/hooks.c
 * Xen side channel mitigation module
 *
 * created on: Jun 21, 2018
 * author: aasthakm
 */

#include "ptcp_hooks_impl.h"
#include <linux/skbuff.h>
#include <uapi/linux/tcp.h>
#include <uapi/linux/in.h>
#include <uapi/linux/ip.h>
#include <uapi/asm-generic/errno-base.h>

void (*lnk_print_sock_skb) (struct sock *sk, struct sk_buff *skb, char *dbg_str) = 0;
EXPORT_SYMBOL(lnk_print_sock_skb);
static void
ptcp_impl_print_sock_skb(struct sock *sk, struct sk_buff *skb, char *dbg_str)
{
  if (lnk_print_sock_skb) {
    lnk_print_sock_skb(sk, skb, dbg_str);
  }
}

int (*lnk_rx_pred_flags) (struct sock *sk, struct sk_buff *skb, struct tcphdr *th) = 0;
EXPORT_SYMBOL(lnk_rx_pred_flags);
static int ptcp_impl_rx_pred_flags(struct sock *sk, struct sk_buff *skb,
    struct tcphdr *th)
{
  if (lnk_rx_pred_flags) {
    return lnk_rx_pred_flags(sk, skb, th);
  }

  return -1;
}

int (*lnk_rx_handle_tcp_urg) (struct sock *sk, struct sk_buff *skb,
    struct tcphdr *th) = 0;
EXPORT_SYMBOL(lnk_rx_handle_tcp_urg);
static int
ptcp_impl_rx_handle_tcp_urg(struct sock *sk, struct sk_buff *skb, struct tcphdr *th)
{
  if (lnk_rx_handle_tcp_urg) {
    return lnk_rx_handle_tcp_urg(sk, skb, th);
  }

  return -1;
}

int (*lnk_rx_handle_dummy) (struct sock *sk, struct sk_buff *skb) = 0;
EXPORT_SYMBOL(lnk_rx_handle_dummy);
static int
ptcp_impl_rx_handle_dummy(struct sock *sk, struct sk_buff *skb)
{
  if (lnk_rx_handle_dummy) {
    return lnk_rx_handle_dummy(sk, skb);
  }

  return -1;
}

int (*lnk_rx_disable_coalesce) (struct sock *sk, struct sk_buff *to,
    struct sk_buff *from) = 0;
EXPORT_SYMBOL(lnk_rx_disable_coalesce);
static int
ptcp_impl_rx_disable_coalesce(struct sock *sk, struct sk_buff *to,
    struct sk_buff *from)
{
  if (lnk_rx_disable_coalesce) {
    return lnk_rx_disable_coalesce(sk, to, from);
  }

  return -1;
}

int (*lnk_rx_adjust_skb_size) (struct sock *sk, struct sk_buff *skb, int req_len,
    int offset, int chunk, int copied, int flags) = 0;
EXPORT_SYMBOL(lnk_rx_adjust_skb_size);
static int
ptcp_impl_rx_adjust_skb_size(struct sock *sk, struct sk_buff *skb, int req_len,
    int offset, int chunk, int copied, int flags)
{
  if (lnk_rx_adjust_skb_size) {
    return lnk_rx_adjust_skb_size(sk, skb, req_len, offset, chunk,
        copied, flags);
  }

  return -1;
}

int (*lnk_rx_adjust_copied_seq) (struct sock *sk, struct sk_buff *skb,
    int old_skb_len, int new_skb_len) = 0;
EXPORT_SYMBOL(lnk_rx_adjust_copied_seq);
static int
ptcp_impl_rx_adjust_copied_seq (struct sock *sk, struct sk_buff *skb,
    int old_skb_len, int new_skb_len)
{
  if (lnk_rx_adjust_copied_seq) {
    return lnk_rx_adjust_copied_seq(sk, skb, old_skb_len, new_skb_len);
  }

  return -1;
}

int (*lnk_tx_adjust_seq) (struct sock *sk, struct sk_buff *skb, int copy,
    int copied) = 0;
EXPORT_SYMBOL(lnk_tx_adjust_seq);
static int
ptcp_impl_tx_adjust_seq(struct sock *sk, struct sk_buff *skb, int copy, int copied)
{
  if (lnk_tx_adjust_seq) {
    return lnk_tx_adjust_seq(sk, skb, copy, copied);
  }

  return -1;
}

int (*lnk_rxtx_sync_shared_seq) (struct sock *sk, struct sk_buff *skb, int write) = 0;
EXPORT_SYMBOL(lnk_rxtx_sync_shared_seq);
static int
ptcp_impl_rxtx_sync_shared_seq(struct sock *sk, struct sk_buff *skb, int write)
{
  if (lnk_rxtx_sync_shared_seq) {
    return lnk_rxtx_sync_shared_seq(sk, skb, write);
  }

  return -1;
}

int (*lnk_tx_adjust_urg) (struct sock *sk, struct sk_buff *skb) = 0;
EXPORT_SYMBOL(lnk_tx_adjust_urg);
static int
ptcp_impl_tx_adjust_urg(struct sock *sk, struct sk_buff *skb)
{
  if (lnk_tx_adjust_urg) {
    return lnk_tx_adjust_urg(sk, skb);
  }

  return -1;
}

int (*lnk_tx_incr_tail) (struct sock *sk, struct sk_buff *skb) = 0;
EXPORT_SYMBOL(lnk_tx_incr_tail);
static int
ptcp_impl_tx_incr_tail(struct sock *sk, struct sk_buff *skb)
{
  if (lnk_tx_incr_tail) {
    return lnk_tx_incr_tail(sk, skb);
  }

  return -1;
}

int (*lnk_tx_incr_profile) (struct sock *sk, struct sk_buff *skb) = 0;
EXPORT_SYMBOL(lnk_tx_incr_profile);
static int
ptcp_impl_tx_incr_profile(struct sock *sk, struct sk_buff *skb)
{
  if (lnk_tx_incr_profile) {
    return lnk_tx_incr_profile(sk, skb);
  }

  return -1;
}

int (*lnk_is_paced_flow) (struct sock *sk) = 0;
EXPORT_SYMBOL(lnk_is_paced_flow);
static int
ptcp_impl_is_paced_flow(struct sock *sk)
{
  if (lnk_is_paced_flow) {
    return lnk_is_paced_flow(sk);
  }

  return -1;
}

void (*lnk_timer_rto_check) (struct sock *sk) = 0;
EXPORT_SYMBOL(lnk_timer_rto_check);
static void
ptcp_impl_timer_rto_check(struct sock *sk)
{
  if (lnk_timer_rto_check) {
    return lnk_timer_rto_check(sk);
  }
}

int (*lnk_get_retransmit_skb) (struct sock *sk, struct sk_buff **skb_p) = 0;
EXPORT_SYMBOL(lnk_get_retransmit_skb);
static int
ptcp_impl_get_retransmit_skb(struct sock *sk, struct sk_buff **skb_p)
{
  if (lnk_get_retransmit_skb) {
    return lnk_get_retransmit_skb(sk, skb_p);
  }

  return -1;
}

int (*lnk_build_rexmit_dummy) (struct sock *sk, struct sk_buff **skb_p,
    uint32_t seqno, uint32_t seqack, int sacked_flags) = 0;
EXPORT_SYMBOL(lnk_build_rexmit_dummy);
static int
ptcp_impl_build_rexmit_dummy(struct sock *sk, struct sk_buff **skb_p,
    uint32_t seqno, uint32_t seqack, int sacked_flags)
{
  if (lnk_build_rexmit_dummy) {
    return lnk_build_rexmit_dummy(sk, skb_p, seqno, seqack, sacked_flags);
  }

  return -1;
}

struct ptcp_hook_list ptcp_hooks[NUM_PTCP_HOOKS] = {
  PTCP_HOOK_INIT(print_sock_skb, ptcp_impl_print_sock_skb),
  PTCP_HOOK_INIT(rx_pred_flags, ptcp_impl_rx_pred_flags),
  PTCP_HOOK_INIT(rx_handle_tcp_urg, ptcp_impl_rx_handle_tcp_urg),
  PTCP_HOOK_INIT(rx_handle_dummy, ptcp_impl_rx_handle_dummy),
  PTCP_HOOK_INIT(rx_disable_coalesce, ptcp_impl_rx_disable_coalesce),
  PTCP_HOOK_INIT(rx_adjust_skb_size, ptcp_impl_rx_adjust_skb_size),
  PTCP_HOOK_INIT(rx_adjust_copied_seq, ptcp_impl_rx_adjust_copied_seq),
  PTCP_HOOK_INIT(tx_adjust_seq, ptcp_impl_tx_adjust_seq),
  PTCP_HOOK_INIT(rxtx_sync_shared_seq, ptcp_impl_rxtx_sync_shared_seq),
  PTCP_HOOK_INIT(tx_adjust_urg, ptcp_impl_tx_adjust_urg),
  PTCP_HOOK_INIT(tx_incr_tail, ptcp_impl_tx_incr_tail),
  PTCP_HOOK_INIT(tx_incr_profile, ptcp_impl_tx_incr_profile),
  PTCP_HOOK_INIT(is_paced_flow, ptcp_impl_is_paced_flow),
  PTCP_HOOK_INIT(timer_rto_check, ptcp_impl_timer_rto_check),
  PTCP_HOOK_INIT(get_retransmit_skb, ptcp_impl_get_retransmit_skb),
  PTCP_HOOK_INIT(build_rexmit_dummy, ptcp_impl_build_rexmit_dummy),
};

#if 0
static int __init ptcp_init(void)
{
	iprintk(0, "PTCP: Initializing");
	ptcp_add_hooks(ptcp_hooks, ARRAY_SIZE(ptcp_hooks));
	return 0;
}

module_init(ptcp_init);

static void __exit ptcp_fini(void)
{
	ptcp_delete_hooks(ptcp_hooks, ARRAY_SIZE(ptcp_hooks));
	iprintk(0, "PTCP complete");
}

module_exit(ptcp_fini);
MODULE_LICENSE("Dual BSD/GPL");
MODULE_ALIAS("pacerclient");
#endif
