/*
 * xen_sme_hooks.c
 *
 * created on: Feb 25, 2017
 * author: aasthakm
 *
 * Similar to security.h/c
 * symbols exported to be used in the netback driver
 * 
 */

#include "xen_sme_hooks.h"
#include "xen_sme.h"

#define call_sme_void_hook(FUNC, ...)	({	\
		do {	\
			struct sme_hook_list *P;	\
			list_for_each_entry(P, &xen_sme_hook_heads.FUNC, list)	{	\
				P->hook.FUNC(__VA_ARGS__);	\
			}	\
		} while (0);	\
	})

#define call_sme_int_hook(FUNC, IRC, ...)	({	\
		int RC = IRC;	\
		do {	\
			struct sme_hook_list *P;	\
			list_for_each_entry(P, &xen_sme_hook_heads.FUNC, list)	{	\
				RC = P->hook.FUNC(__VA_ARGS__);	\
				if (RC != 0)	\
					break;	\
			}	\
		} while (0);	\
		RC;	\
	})

struct sme_hook_heads xen_sme_hook_heads = {
	.update_cwnd = LIST_HEAD_INIT(xen_sme_hook_heads.update_cwnd),
  .intercept_select_queue =
    LIST_HEAD_INIT(xen_sme_hook_heads.intercept_select_queue),
  .intercept_tx_sent_queue =
    LIST_HEAD_INIT(xen_sme_hook_heads.intercept_tx_sent_queue),
  .intercept_tx_completed_queue =
    LIST_HEAD_INIT(xen_sme_hook_heads.intercept_tx_completed_queue),
	.intercept_sk_buff = LIST_HEAD_INIT(xen_sme_hook_heads.intercept_sk_buff),
  .intercept_xmit_stop_queue =
    LIST_HEAD_INIT(xen_sme_hook_heads.intercept_xmit_stop_queue),
  .intercept_tx_int = LIST_HEAD_INIT(xen_sme_hook_heads.intercept_tx_int),
  .intercept_free_tx_skbs_queue =
    LIST_HEAD_INIT(xen_sme_hook_heads.intercept_free_tx_skbs_queue),
	.parse_rx_data = LIST_HEAD_INIT(xen_sme_hook_heads.parse_rx_data),
	.print_sk_buff = LIST_HEAD_INIT(xen_sme_hook_heads.print_sk_buff),
	.intercept_rx_path = LIST_HEAD_INIT(xen_sme_hook_heads.intercept_rx_path),
	.print_rx_data = LIST_HEAD_INIT(xen_sme_hook_heads.print_rx_data),
};

void xsl_update_cwnd(struct sk_buff *skb, struct sock *sk, int caller,
    int b_cwnd, int b_pkts, int b_lost, int b_retrans, int b_sack,
    int a_cwnd, int a_pkts, int a_lost, int a_retrans, int a_sack,
    int una_diff, int delivered, int ack_flag, int num_marked_lost,
    char *extra_dbg_string)
{
  return call_sme_void_hook(update_cwnd, skb, sk, caller,
      b_cwnd, b_pkts, b_lost, b_retrans, b_sack,
      a_cwnd, a_pkts, a_lost, a_retrans, a_sack,
      una_diff, delivered, ack_flag, num_marked_lost, extra_dbg_string);
}
EXPORT_SYMBOL(xsl_update_cwnd);

int xsl_intercept_select_queue(struct net_device *dev, struct sk_buff *skb,
    char *extra_dbg_string)
{
  return call_sme_int_hook(intercept_select_queue, -1, dev, skb, extra_dbg_string);
}
EXPORT_SYMBOL(xsl_intercept_select_queue);

void xsl_intercept_tx_sent_queue(struct netdev_queue *txq, unsigned int bytes)
{
  call_sme_void_hook(intercept_tx_sent_queue, txq, bytes);
}
EXPORT_SYMBOL(xsl_intercept_tx_sent_queue);

void xsl_intercept_tx_completed_queue(struct netdev_queue *txq, unsigned int pkts,
    unsigned int bytes)
{
  return call_sme_void_hook(intercept_tx_completed_queue, txq, pkts, bytes);
}
EXPORT_SYMBOL(xsl_intercept_tx_completed_queue);

/*
 * use -1 as default return value, since we want to bypass interception
 * code when return value is < 0
 */
int xsl_intercept_sk_buff(struct sk_buff *skb, char *extra_dbg_string)
{
	return call_sme_int_hook(intercept_sk_buff, -1, skb, extra_dbg_string);
}
EXPORT_SYMBOL(xsl_intercept_sk_buff);

void xsl_intercept_xmit_stop_queue(struct net_device *dev, struct netdev_queue *txq,
    struct bnx2x_fp_txdata *txdata)
{
  return call_sme_void_hook(intercept_xmit_stop_queue, dev, txq, txdata);
}
EXPORT_SYMBOL(xsl_intercept_xmit_stop_queue);

int xsl_intercept_tx_int(struct net_device *dev, struct bnx2x_fp_txdata *txdata)
{
  return call_sme_int_hook(intercept_tx_int, -1, dev, txdata);
}
EXPORT_SYMBOL(xsl_intercept_tx_int);

void xsl_intercept_free_tx_skbs_queue(struct bnx2x_fastpath *fp)
{
  return call_sme_void_hook(intercept_free_tx_skbs_queue, fp);
}
EXPORT_SYMBOL(xsl_intercept_free_tx_skbs_queue);

void xsl_parse_rx_data(void *dev, int fp_idx, uint16_t rx_bd_prod,
		uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons,
		char *data, int data_len, char *extra_dbg_string)
{
	return call_sme_void_hook(parse_rx_data, dev, fp_idx, rx_bd_prod, rx_bd_cons, comp_prod,
			comp_cons, data, data_len, extra_dbg_string);
}
EXPORT_SYMBOL(xsl_parse_rx_data);

void xsl_print_sk_buff(struct sk_buff *skb, struct sock *sk, char *extra_dbg_string)
{
	return call_sme_void_hook(print_sk_buff, skb, sk, extra_dbg_string);
}
EXPORT_SYMBOL(xsl_print_sk_buff);

void xsl_intercept_rx_path(void *dev, int fp_idx, uint16_t rx_bd_prod,
		uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons, int irq,
		char *extra_dbg_string)
{
	return call_sme_void_hook(intercept_rx_path, dev, fp_idx, rx_bd_prod,
			rx_bd_cons, comp_prod, comp_cons, irq, extra_dbg_string);
}
EXPORT_SYMBOL(xsl_intercept_rx_path);

void xsl_print_rx_data(void *dev, int fp_idx, uint16_t rx_bd_prod,
		uint16_t rx_bd_cons, uint16_t rx_comp_prod, uint16_t rx_comp_cons,
		char *extra_dbg_string)
{
	return call_sme_void_hook(print_rx_data, dev, fp_idx, rx_bd_prod,
			rx_bd_cons, rx_comp_prod, rx_comp_cons, extra_dbg_string);
}
EXPORT_SYMBOL(xsl_print_rx_data);

